#include <cmath>

#include "Game.hpp"
#include "Baddie.hpp"

const irr::u32 BADDIE_FOV = 180,BADDIE_FOV_ATTENTION = 60,SCAN_RANGE = 160;
const irr::u32 BADDIE_INSPECTING_TIME_MIN = 6000,BADDIE_INSPECTING_TIME_MAX = 15000;

const irr::u32 BADDIE_FULL_SPEED = 5;
const irr::f32 BADDIE_STOP_RANGE = 1.5;

Baddie::Baddie(irr::core::vector3df NewPosition)
:State(WANDERING_BADDIE_STATE),Attention(0),Timer(0),Suspicion(-0){
	Pivot = new PureNode(Game::SDAS.Scene->getRootSceneNode(),Game::SDAS.Scene,0);
	Pivot->setPosition(NewPosition);
	
	Mesh = Game::SDAS.Scene->addAnimatedMeshSceneNode(Game::SDAS.Scene->getMesh("Meshes/RawrHorse.b3d"),Pivot,8);
	Mesh->setMaterialTexture(0,Game::SDAS.Video->getTexture("Meshes/Rawr.png"));
	
	Mesh->setScale(irr::core::vector3df(0.5,0.5,0.5));
	Mesh->setMaterialFlag(irr::video::EMF_LIGHTING,false);
	Mesh->addShadowVolumeSceneNode(0,0);
	
	//Mesh->setJointMode(irr::scene::EJUOR_CONTROL);
	//Jaw = Mesh->getJointNode("Jaw");
}

void Baddie::Update(Player *Seeked){
	if(Seeked){
		SightLine.start = Pivot->getPosition();
		SightLine.end = Seeked->Camera->getPosition();
		
		irr::scene::ISceneNode *Seen = Game::SDAS.Scene->getSceneCollisionManager()->getSceneNodeFromRayBB(SightLine,4);
		irr::f32 SightAngle = -1;
		
		if(Seen){
			SightAngle = (Pivot->getRotation() - SightLine.getVector().getHorizontalAngle()).getDistanceFrom(irr::core::vector3df(0,0,0));
			SightAngle = ((SightAngle < 0) * -1) + ((SightAngle > BADDIE_FOV) * -1);
			
			if(SightAngle != -1){
				SightAngle = (SightAngle < BADDIE_FOV_ATTENTION) + ((SightAngle >= BADDIE_FOV_ATTENTION) * (1 - ((SightAngle - BADDIE_FOV_ATTENTION) / (BADDIE_FOV - BADDIE_FOV_ATTENTION))));
			}
		}
		
		/*if(SightAngle != -1){
			if(Seeked->Pumpkin){
				Suspicion += Seeked->GetSecondDeltaPosition() * SightAngle * (Game::SDAS.DeltaTime / 1000);
			}else{
				Suspicion += 0.2 * SightAngle * (Game::SDAS.DeltaTime / 1000);
			}
		}else{*/
			switch(State){
				case WANDERING_BADDIE_STATE:
					Suspicion -= Suspicion * 0.1 * (Game::SDAS.DeltaTime / 1000);
					
					TargetRotation = (TargetPosition - Pivot->getPosition()).getHorizontalAngle();
					
					break;
				case INSPECTING_BADDIE_STATE:
					TargetRotation = Pivot->getRotation() + irr::core::vector3df(0,sin(Game::SDAS.TotalTime / 1000) * (SCAN_RANGE / 2),0);
					Timer -= Game::SDAS.DeltaTime;
					
					//printf("Timer: %f: Delta Time: %f\n",Timer,Game::SDAS.DeltaTime);
					
					if(Timer <= 0){
						State = WANDERING_BADDIE_STATE;
						TargetPosition = irr::core::vector3df(rand() % 10,1 + (rand() % 5),rand() % 10);
						printf("NewTarget: %f, %f, %f\n",TargetPosition.X,TargetPosition.Y,TargetPosition.Z);
					}
					
					break;
			}
			
			//printf("Target: %f, %f, %f\n",TargetPosition.X,TargetPosition.Y,TargetPosition.Z);
			//printf("Current: %f, %f, %f\n",Pivot->getPosition().X,Pivot->getPosition().Y,Pivot->getPosition().Z);
			
			if((State == WANDERING_BADDIE_STATE || State == ATTACKING_BADDIE_STATE) && Pivot->getPosition().getDistanceFrom(TargetPosition) <= 0.2){
				State = INSPECTING_BADDIE_STATE;
				Timer = BADDIE_INSPECTING_TIME_MIN + (rand() % (BADDIE_INSPECTING_TIME_MAX - BADDIE_INSPECTING_TIME_MIN));
			}
		//}
	}
	
	//printf("State: %i\n",(int)State);
	
	Pivot->setPosition(Pivot->getPosition() + ((TargetPosition - Pivot->getPosition()).normalize() * (Game::SDAS.DeltaTime / 1000)));
	Pivot->setRotation(Pivot->getRotation() + ((TargetRotation - Pivot->getRotation()) * (Game::SDAS.DeltaTime / 1000)));
	
	//Mesh->setPosition(irr::core::vector3df(0,sin(Game::SDAS.TotalTime / 500.0) * 0.3,0));
	//Jaw->setRotation(irr::core::vector3df(280,0,0));//sin(Game::SDAS.TotalTime / 500.0) * 0.3,0,0));
}