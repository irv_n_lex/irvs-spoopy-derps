#ifndef UTILS_INC
	#include <irrlicht.h>
	
	class PureNode : public irr::scene::ISceneNode{
		public:
			PureNode(irr::scene::ISceneNode *Parent,irr::scene::ISceneManager *Manager,irr::s32 ID);
			
			virtual void OnRegisterSceneNode();
			virtual void render();
			virtual const irr::core::aabbox3d<irr::f32> &getBoundingBox() const;
			virtual irr::u32 getMaterialCount() const;
	};
	
	inline void KillIrrlichtDevice(irr::IrrlichtDevice* &ExecutedDevice){
		ExecutedDevice->closeDevice();
		ExecutedDevice->run();
		ExecutedDevice->drop();
	}
	
	#define UTILS_INC
#endif
