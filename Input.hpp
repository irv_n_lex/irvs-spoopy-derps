#ifndef INPUT_INC
	#include <irrlicht.h>
	
	enum MOUSE_BUTTON{
		MOUSE_LEFT = 0,
		MOUSE_RIGHT = 1,
		MOUSE_MIDDLE = 2,
		MOUSE_BUTTON_COUNT = 3
	};
	
	class InputManager : public irr::IEventReceiver{
		private:
			bool KeyIsDown[irr::KEY_KEY_CODES_COUNT],KeyWasDown[irr::KEY_KEY_CODES_COUNT];
			bool MouseIsDown[3],MouseWasDown[3];
			
			irr::f32 MouseWheelMovement;
			
			virtual bool OnEvent(const irr::SEvent& Event);
			
		public:
			irr::f32 MouseX,MouseY;
			
			InputManager();
			void Update();
			
			bool KeyHit(irr::EKEY_CODE KeyCode);
			bool KeyDown(irr::EKEY_CODE KeyCode);
			bool KeyReleased(irr::EKEY_CODE KeyCode);
			
			bool MouseHit(MOUSE_BUTTON MouseButton);
			bool MouseDown(MOUSE_BUTTON MouseButton);
			bool MouseReleased(MOUSE_BUTTON MouseButton);
	};
	#define INPUT_INC
#endif
