#ifndef RESOURCES_INC
	#include <vector>
	
	#include "irrlicht.h"
	
	enum TextureResource{
		LOLLI_RED_TEXTURE,
		LOLLI_GREEN_TEXTURE,
		
		CANDEH_RED_TEXTURE,
		CANDEH_PURPLE_TEXTURE,
		CANDEH_YELLOW_TEXTURE,
		CANDEH_AQUA_TEXTURE,
		
		STICK_PINK_TEXTURE,
		STICK_YELLOW_TEXTURE,
		STICK_GREEN_TEXTURE,
		
		TEXTURE_RESOURCE_COUNT
	};
	
	enum MeshResource{
		HOUSE_GENERIC_MESH,
		HOUSE_TOWER_MESH,
		
		CANDY_CORNZ_MESH,
		LOLLIPOP_MESH,
		CANDEH_MESH,
		SUGAH_STICK_MESH,
		
		PUMPKIN_YAY_MESH,
		PUMPKIN_OH_MESH,
		PUMPKIN_EVIL_MESH,
		PUMPKIN_MOON_MESH,
		
		MESH_RESOURCE_COUNT
	};
	
	class Resources{
		private:
			std::vector<irr::video::ITexture*> Textures;
			std::vector<irr::scene::IAnimatedMesh*> Meshes;
			
		public:
			Resources();
			~Resources();
			
			irr::video::ITexture *GetTexture(TextureResource RequestedTexture);
			irr::scene::IAnimatedMesh *GetMesh(MeshResource RequestedMesh);
	};
	
	#define RESOURCES_INC
#endif