#include <irrlicht.h>

#include "Utils.hpp"

PureNode::PureNode(irr::scene::ISceneNode *Parent,irr::scene::ISceneManager *Manager,irr::s32 ID = -1) : irr::scene::ISceneNode(Parent,Manager,ID){
	setAutomaticCulling(irr::scene::EAC_OFF);
}

void PureNode::OnRegisterSceneNode(){
	SceneManager->registerNodeForRendering(this);
	ISceneNode::OnRegisterSceneNode();
}

void PureNode::render(){
	//lol nothing happens lol
}

const irr::core::aabbox3d<irr::f32> &PureNode::getBoundingBox() const{
	static irr::core::aabbox3d<irr::f32> Nah(irr::core::vector3df(0,0,0));
	return Nah;
}

irr::u32 PureNode::getMaterialCount() const{
	return 1;
}