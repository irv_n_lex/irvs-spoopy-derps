#ifndef OBJECTS_INC
	#include "Entity.hpp"
	
	enum ObjectType{
		CANDY_OBJECT,
		PUMPKIN_OBJECT
	};
	
	enum CandyType{
		CANDY_CORNZ_TYPE,
		LOLLIPOP_TYPE,
		CANDEH_TYPE,
		SUGAR_STICK_TYPE,
		
		CANDY_TYPE_COUNT
	};
	
	enum PumpkinType{
		PUMPKIN_YAY_TYPE,
		PUMPKIN_OH_TYPE,
		PUMPKIN_EVIL_TYPE,
		PUMPKIN_MOON_TYPE,
		
		PUMPKIN_TYPE_COUNT
	};
	
	struct Object: public Entity{
		ObjectType Type;
		int Var;
		irr::scene::IMeshSceneNode *Mesh;
		
		Object(ObjectType NewType,int NewVar = -1,irr::core::vector3df NewPosition = irr::core::vector3df(0,0,0),irr::core::vector3df NewRotation = irr::core::vector3df(0,0,0));
		~Object();
	};
	
	#define OBJECTS_INC
#endif