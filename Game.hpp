#ifndef GAME_INC
	#include <vector>
	
	#include <irrlicht.h>
	
	#include "Input.hpp"
	#include "Resources.hpp"
	#include "Player.hpp"
	#include "Baddie.hpp"
	#include "Objects.hpp"
	
	class Game{
		private:
			irr::IrrlichtDevice *Device;
			
			irr::u32 BeginTime;
			
			Player *NotBadHorse;
			Baddie *BadHorse;
			
			Game();
			
		public:
			irr::ITimer *Timer;
			irr::gui::ICursorControl *Cursor;
			irr::scene::ISceneManager *Scene;
			irr::video::IVideoDriver *Video;
			irr::gui::IGUIEnvironment *GUI;
			InputManager *Input;
			Resources *Stuffs;
			
			irr::f32 DeltaTime;
			irr::u32 TotalTime;
			irr::core::vector2df DeltaCursorPosition;
			
			irr::scene::ITerrainSceneNode *Ground;
			std::vector<Object*> Objects;
			std::vector<irr::scene::IMeshSceneNode*> Hice;
			
			static Game SDAS;
			
			void Run();
			void SpawnObject(ObjectType NewType,int NewVar = -1,irr::core::vector3df NewPosition = irr::core::vector3df(0,0,0),irr::core::vector3df NewRotation = irr::core::vector3df(0,0,0));
			void GenerateTown();
			void ApplyCollisions();
	};
	
	#define GAME_INC
#endif