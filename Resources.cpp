#include <iterator>

#include "Game.hpp"
#include "Resources.hpp"

Resources::Resources(){
	Textures.resize(TEXTURE_RESOURCE_COUNT);
	
	for(std::vector<irr::video::ITexture*>::iterator IT = Textures.begin();IT != Textures.end();++IT){
		irr::s32 I = std::distance(Textures.begin(),IT);
		
		switch(I){
			case LOLLI_RED_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/LolliRed.png");
				break;
			case LOLLI_GREEN_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/LolliGreen.png");
				break;
			case CANDEH_RED_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/CandehRed.png");
				break;
			case CANDEH_PURPLE_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/CandehPurple.png");
				break;
			case CANDEH_YELLOW_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/CandehYellow.png");
				break;
			case CANDEH_AQUA_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/CandehAqua.png");
				break;
			case STICK_PINK_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/StickPink.png");
				break;
			case STICK_YELLOW_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/StickYellow.png");
				break;
			case STICK_GREEN_TEXTURE:
				Textures[I] = Game::SDAS.Video->getTexture("Meshes/Candies/StickGreen.png");
				break;
		}
	}
	
	Meshes.resize(MESH_RESOURCE_COUNT);
	
	for(std::vector<irr::scene::IAnimatedMesh*>::iterator IT = Meshes.begin();IT != Meshes.end();++IT){
		irr::s32 I = std::distance(Meshes.begin(),IT);
		
		switch(I){
			case HOUSE_GENERIC_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Town/House.obj");
				break;
			case HOUSE_TOWER_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Town/Tower.obj");
				break;
			case CANDY_CORNZ_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Candies/CandyCornz.obj");
				break;
			case LOLLIPOP_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Candies/Lollipop.obj");
				break;
			case CANDEH_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Candies/Candeh.obj");
				break;
			case SUGAH_STICK_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Candies/SugahStick.obj");
				break;
			case PUMPKIN_YAY_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Pumpkins/PumpkinYay.obj");
				break;
			case PUMPKIN_OH_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Pumpkins/PumpkinOh.obj");
				break;
			case PUMPKIN_EVIL_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Pumpkins/PumpkinEvil.obj");
				break;
			case PUMPKIN_MOON_MESH:
				Meshes[I] = Game::SDAS.Scene->getMesh("Meshes/Pumpkins/PumpkinMoon.obj");
				break;
		}
	}
}

Resources::~Resources(){
	
}

irr::video::ITexture *Resources::GetTexture(TextureResource RequestedTexture){
	return Textures[RequestedTexture];
}

irr::scene::IAnimatedMesh *Resources::GetMesh(MeshResource RequestedMesh){
	return Meshes[RequestedMesh];
}