#ifndef PLAYER_INC
	#include <irrlicht.h>
	
	#include "Entity.hpp"
	#include "Objects.hpp"
	#include "Utils.hpp"
	
	class Player: public Entity{
		friend class Game;
		friend class Baddie; //lol dat irony
		
		private:
			irr::scene::ICameraSceneNode *Camera;
			PureNode *CameraMovementTarget,*CameraTarget,*PumpkinPivot;
			irr::core::vector3df Rotation,TargetRotation,PumpkinRotation;
			
			irr::scene::ISceneNode *Focused,*OldFocus;
			Object *Pumpkin;
			
			irr::u32 CandyCount;
			irr::f32 DeltaPosition;
			
		public:
			Player(irr::core::vector3df NewPosition = irr::core::vector3df(0,0,0));
			
			void Update();
			
			irr::f32 GetSecondDeltaPosition();
	};
	
	#define PLAYER_INC
#endif