#ifndef BADDIE_INC
	#include <irrlicht.h>
	
	#include "Player.hpp"
	#include "Utils.hpp"
	
	enum BaddieState{
		WANDERING_BADDIE_STATE,
		INSPECTING_BADDIE_STATE,
		ATTACKING_BADDIE_STATE
	};
	
	class Baddie{
		private:
			BaddieState State;
			
			PureNode *Pivot;
			irr::scene::IAnimatedMeshSceneNode *Mesh;
			irr::scene::IBoneSceneNode* Jaw;
			irr::core::line3df SightLine;
			
			irr::core::vector3df TargetPosition,TargetRotation,Attention;
			irr::f32 Timer,Suspicion;
			
		public:
			Baddie(irr::core::vector3df NewPosition = irr::core::vector3df(0,0,0));
			
			void Update(Player *Seeked = 0);
	};
	
	#define BADDIE_INC
#endif