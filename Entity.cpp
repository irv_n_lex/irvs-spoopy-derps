#include "Game.hpp"
#include "Entity.hpp"

const irr::f32 GRAVITY_FORCE = -0.1;//2f;

Entity::Entity()
:Collider(0),Slidiness(0),GravitySet(false){
	
}

void Entity::SetCollSize(irr::core::vector3df NewSize){
	CollSize = NewSize;
}

void Entity::SetCollTranslation(irr::core::vector3df NewTranslation){
	CollTranslation = NewTranslation;
}

void Entity::SetSlidiness(irr::f32 NewSlidiness){
	Slidiness = NewSlidiness;
}

bool Entity::ApplyCollsWith(irr::scene::IMeshSceneNode *NewColl){
	if(Collider && NewColl){
		irr::scene::ITriangleSelector *CollSelector = Game::SDAS.Scene->createTriangleSelector(NewColl->getMesh(),NewColl);
		NewColl->setTriangleSelector(CollSelector);
		
		irr::scene::ISceneNodeAnimator *CollisionAnim = Game::SDAS.Scene->createCollisionResponseAnimator(
			CollSelector,
			Collider,
			CollSize,
			irr::core::vector3df(0,(!GravitySet) * GRAVITY_FORCE,0),
			CollTranslation,
			Slidiness
		);
		
		CollSelector->drop();
		Collider->addAnimator(CollisionAnim);
		CollisionAnim->drop();
		
		if(!GravitySet){
			GravitySet = true;
		}
		
		return true;
	}
	
	return false;
}

bool Entity::ApplyCollsWith(irr::scene::ITerrainSceneNode *NewColl){
	if(Collider && NewColl){
		irr::scene::ITriangleSelector *CollSelector = Game::SDAS.Scene->createTerrainTriangleSelector(NewColl,0);
		NewColl->setTriangleSelector(CollSelector);
		
		irr::scene::ISceneNodeAnimator *CollisionAnim = Game::SDAS.Scene->createCollisionResponseAnimator(
			CollSelector,
			Collider,
			CollSize,
			irr::core::vector3df(0,(!GravitySet) * GRAVITY_FORCE,0),
			CollTranslation,
			Slidiness
		);
		
		CollSelector->drop();
		Collider->addAnimator(CollisionAnim);
		CollisionAnim->drop();
		
		if(!GravitySet){
			GravitySet = true;
		}
		
		return true;
	}
	
	return false;
}