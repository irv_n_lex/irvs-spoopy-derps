#ifndef ENTITY_INC
	#include <irrlicht.h>
	
	//const irr::u32 PLAYER_COLL_TYPE = 2,OBJECT_;
	
	class Entity{
		protected:
			irr::scene::ISceneNode *Collider;
			
			irr::core::vector3df CollSize,CollTranslation;
			irr::f32 Slidiness;
			
			bool GravitySet;
			
		public:
			Entity();
			
			void SetCollEnv(irr::scene::IMeshSceneNode *NewEnv);
			void SetCollSize(irr::core::vector3df NewSize);
			void SetCollTranslation(irr::core::vector3df NewTranslation);
			void SetSlidiness(irr::f32 NewSlidiness);
			
			bool ApplyCollsWith(irr::scene::IMeshSceneNode *NewColl);
			bool ApplyCollsWith(irr::scene::ITerrainSceneNode *NewColl);
	};
	
	#define ENTITY_INC
#endif