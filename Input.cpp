#include <irrlicht.h>

#include "Input.hpp"

InputManager::InputManager(){
	for(irr::u32 I = 0;I < irr::KEY_KEY_CODES_COUNT;++I){
		KeyIsDown[I] = false;
		KeyWasDown[I] = false;
	}
	
	for(irr::u32 I = 0;I < MOUSE_BUTTON_COUNT;++I){
		MouseIsDown[I] = false;
		MouseWasDown[I] = false;
	}
}

bool InputManager::OnEvent(const irr::SEvent& Event){
	if(Event.EventType == irr::EET_KEY_INPUT_EVENT){
		KeyIsDown[Event.KeyInput.Key] = Event.KeyInput.PressedDown;
	}else if(Event.EventType == irr::EET_MOUSE_INPUT_EVENT){
		switch(Event.MouseInput.Event){
			case irr::EMIE_MOUSE_MOVED:
				MouseX = Event.MouseInput.X;
				MouseY = Event.MouseInput.Y;
				break;
			case irr::EMIE_MOUSE_WHEEL:
				MouseWheelMovement = Event.MouseInput.Wheel;
				break;
			case irr::EMIE_LMOUSE_PRESSED_DOWN:
				MouseIsDown[MOUSE_LEFT] = true;
				break;
			case irr::EMIE_RMOUSE_PRESSED_DOWN:
				MouseIsDown[MOUSE_RIGHT] = true;
				break;
			case irr::EMIE_MMOUSE_PRESSED_DOWN:
				MouseIsDown[MOUSE_MIDDLE] = true;
				break;
			case irr::EMIE_LMOUSE_LEFT_UP:
				MouseIsDown[MOUSE_LEFT] = false;
				break;
			case irr::EMIE_RMOUSE_LEFT_UP:
				MouseIsDown[MOUSE_RIGHT] = false;
				break;
			case irr::EMIE_MMOUSE_LEFT_UP:
				MouseIsDown[MOUSE_MIDDLE] = false;
				break;
			case irr::EMIE_LMOUSE_DOUBLE_CLICK:
				break;
			case irr::EMIE_RMOUSE_DOUBLE_CLICK:
				break;
			case irr::EMIE_MMOUSE_DOUBLE_CLICK:
				break;
			case irr::EMIE_LMOUSE_TRIPLE_CLICK:
				break;
			case irr::EMIE_RMOUSE_TRIPLE_CLICK:
				break;
			case irr::EMIE_MMOUSE_TRIPLE_CLICK:
				break;
			case irr::EMIE_COUNT: //No practical use.
				break;
		}
	}
	
	return false;
}

void InputManager::Update(){
	for(irr::u32 I = 0;I < irr::KEY_KEY_CODES_COUNT;++I){
		KeyWasDown[I] = KeyIsDown[I];
	}
	
	for(irr::u32 I = 0;I < MOUSE_BUTTON_COUNT;++I){
		MouseWasDown[I] = MouseIsDown[I];
	}
}

bool InputManager::KeyHit(irr::EKEY_CODE KeyCode){
	if(KeyIsDown[KeyCode] && (!KeyWasDown[KeyCode])){
		return true;
	}else{
		return false;
	}
}

bool InputManager::KeyDown(irr::EKEY_CODE KeyCode){
	return KeyIsDown[KeyCode];
}

bool InputManager::KeyReleased(irr::EKEY_CODE KeyCode){
	if(KeyWasDown[KeyCode] && !KeyIsDown[KeyCode]){
		return true;
	}else{
		return false;
	}
}

bool InputManager::MouseHit(MOUSE_BUTTON MouseButton){
	if(MouseIsDown[MouseButton] && (!MouseWasDown[MouseButton])){
		return true;
	}else{
		return false;
	}
}

bool InputManager::MouseDown(MOUSE_BUTTON MouseButton){
	return MouseIsDown[MouseButton];
}

bool InputManager::MouseReleased(MOUSE_BUTTON MouseButton){
	if((!MouseIsDown[MouseButton]) && MouseWasDown[MouseButton]){
		return true;
	}else{
		return false;
	}
}