#include <cstdlib>
#include <limits>
#include <cmath>
#include <ctime>

#include <irrlicht.h>

#include "Game.hpp"
#include "Utils.hpp"

Game Game::SDAS;

Game::Game(){
	
}

void Game::Run(){
	//Setting up a false device for detecting the desktop graphical properties.
	irr::IrrlichtDevice *NullDevice = irr::createDevice(irr::video::EDT_NULL);
	
	irr::core::dimension2d<irr::u32> DesktopResolution = NullDevice->getVideoModeList()->getDesktopResolution();
	irr::s32 DesktopDepth = NullDevice->getVideoModeList()->getDesktopDepth();
	
	KillIrrlichtDevice(NullDevice);
	
	//And now we create the real thing.
	Input = new InputManager;
	Device = irr::createDevice(irr::video::EDT_OPENGL,DesktopResolution,DesktopDepth,false,true,true,Input);
	Device->setWindowCaption(L"So Dark and Scary");
	
	Timer = Device->getTimer();
	Cursor = Device->getCursorControl();
	Scene = Device->getSceneManager();
	Video = Device->getVideoDriver();
	GUI = Device->getGUIEnvironment();
	
	BeginTime = Timer->getRealTime();
	irr::f32 LastTime = 0.0;
	
	Stuffs = new Resources;
	
	Scene->setAmbientLight(irr::video::SColorf(0.03,0.03,0.1));
	
	irr::scene::ILightSceneNode *Light = Scene->addLightSceneNode(0,irr::core::vector3df(20,40,10),irr::video::SColorf(1,1,1),100,1);
	Light->setLightType(irr::video::ELT_DIRECTIONAL);
	
	irr::video::SLight LightData;
	LightData.DiffuseColor = irr::video::SColorf(0.5,0.5,1,1);
	LightData.Direction = irr::core::vector3df(-90,0,0);
	
	Light->setLightData(LightData);
	Light->enableCastShadow(true);
	
	Scene->addSkyBoxSceneNode(
		Video->getTexture("Meshes/Sky/Top.png"),
		Video->getTexture("Meshes/Sky/Bottom.png"),
		Video->getTexture("Meshes/Sky/Side.png"),
		Video->getTexture("Meshes/Sky/Side.png"),
		Video->getTexture("Meshes/Sky/Side.png"),
		Video->getTexture("Meshes/Sky/Side.png")
	);
	
	irr::video::ITexture *HeightMap = Game::SDAS.Video->getTexture("Meshes/Terrain.png");
	Ground = Scene->addTerrainSceneNode("Meshes/Terrain.png",0,0,irr::core::vector3df((irr::s32)HeightMap->getOriginalSize().Width / -2,0,(irr::s32)HeightMap->getOriginalSize().Height / -2));
	Ground->setMaterialTexture(0,Game::SDAS.Video->getTexture("Meshes/grass.jpg"));
	
	GenerateTown();
	
	NotBadHorse = new Player(irr::core::vector3df(0,50,0));
	BadHorse = new Baddie(irr::core::vector3df(0,1,0));
	
	ApplyCollisions();
	
	Cursor->setVisible(false);
	
	while(Device->run() && !Input->KeyHit(irr::KEY_ESCAPE)){
		Cursor->setPosition(0.5f,0.5f);
		
		TotalTime = Timer->getRealTime() - BeginTime;
		
		DeltaTime = Timer->getTime() - LastTime;
		LastTime = Timer->getTime();
		
		NotBadHorse->Update();
		BadHorse->Update(NotBadHorse);
		
		Video->beginScene(true,true,irr::video::SColor(255,0,0,0));
		Scene->drawAll();
		Video->endScene();
		
		DeltaCursorPosition = Cursor->getRelativePosition() - irr::core::vector2df(0.5,0.5);
		
		Input->Update();
	}
	
	KillIrrlichtDevice(Device);
	delete Input;
}

void Game::SpawnObject(ObjectType NewType,int NewVar,irr::core::vector3df NewPosition,irr::core::vector3df NewRotation){
	Object *NewObject = new Object(NewType,NewVar,NewPosition,NewRotation);
	Objects.push_back(NewObject);
}

void Game::GenerateTown(){
	for(std::vector<irr::scene::IMeshSceneNode*>::iterator IT = Hice.begin();IT < Hice.end();++IT){
		(*IT)->remove();
		Hice.erase(IT);
	}
	
	if(NotBadHorse){
		NotBadHorse->Pumpkin = 0;
	}
	
	for(std::vector<Object*>::iterator IT = Objects.begin();IT < Objects.end();++IT){
		delete *IT;
		Objects.erase(IT);
	}
	
	irr::u32 OrbitalHouseCount = 0,ThingsCount = 8 + (rand() % 2),MaxThing = 0;
	irr::core::vector3df Pencil,TargetThing,*NearestThings = new irr::core::vector3df[ThingsCount];
	irr::core::CMatrix4<irr::f32> Compass;
	
	for(irr::u32 I = 0;I < ThingsCount;++I){
		NearestThings[I] = irr::core::vector3df(std::numeric_limits<float>::infinity(),std::numeric_limits<float>::infinity(),std::numeric_limits<float>::infinity());
	}
	
	irr::scene::IMeshSceneNode *Tower = Scene->addMeshSceneNode(Stuffs->GetMesh(HOUSE_TOWER_MESH),0,0,irr::core::vector3df(0,Ground->getHeight(0,0) - 0.005,0));
	Tower->setScale(irr::core::vector3df(2,2,2));
	Tower->addShadowVolumeSceneNode(0,0);
	Hice.push_back(Tower);
	
	srand(time(0));
	
	TargetThing.Z = 30;
	Compass.setRotationDegrees(irr::core::vector3df(0,rand() % 360,0));
	Compass.rotateVect(TargetThing);
	
	for(irr::s32 I = 0;I < 2;++I){
		OrbitalHouseCount = 10 + (I * 7) + (rand() % 5);
		
		srand(time(0));
		
		for(irr::s32 J = 0;J < OrbitalHouseCount;++J){
			Pencil = irr::core::vector3d<irr::f32>(0,0,20 + (I * 7) + ((float)((rand() % 20) / 100)));
			
			Compass.setRotationDegrees(irr::core::vector3d<irr::f32>(0,(J * (360 / OrbitalHouseCount)),0));// + ((rand() % 10) - 5),0));
			Compass.rotateVect(Pencil);
			
			MaxThing = -1;
			
			for(irr::u32 K = 0;K < ThingsCount;++K){
				if(MaxThing == -1 || (NearestThings[K].getDistanceFrom(TargetThing) > NearestThings[MaxThing].getDistanceFrom(TargetThing))){
					MaxThing = K;
				}
			}
			
			if(Pencil.getDistanceFrom(TargetThing) <= NearestThings[MaxThing].getDistanceFrom(TargetThing)){
				NearestThings[MaxThing] = Pencil;
			}
			
			irr::scene::IMeshSceneNode *NewHouse = Scene->addMeshSceneNode(Stuffs->GetMesh(HOUSE_GENERIC_MESH),0,0,irr::core::vector3df(Pencil.X,Ground->getHeight(Pencil.X,Pencil.Z) - 0.005,Pencil.Z),Pencil.getHorizontalAngle() + irr::core::vector3df(0,((rand() % 4) * 90) + ((rand() % 10) - 5),0));
			NewHouse->addShadowVolumeSceneNode(0,0);
			Hice.push_back(NewHouse);
		}
	}
	
	irr::u32 RowLength;
	
	srand(time(0));
	
	for(irr::u32 I = 0;I < ThingsCount;++I){
		Compass.setRotationDegrees(NearestThings[I].getHorizontalAngle());
		RowLength = 1 + (rand() % 3);
		
		for(irr::u32 J = 0;J < RowLength;++J){
			Pencil = irr::core::vector3df(0,0,35 + (J * 8));
			Compass.rotateVect(Pencil);
			
			//printf("%f, %f, %f\n",Pencil.X,Pencil.Y,Pencil.Z);
			
			irr::scene::IMeshSceneNode *NewHouse = Scene->addMeshSceneNode(Stuffs->GetMesh(HOUSE_GENERIC_MESH),0,0,irr::core::vector3df(Pencil.X,Ground->getHeight(Pencil.X,Pencil.Z) - 0.005,Pencil.Z),Pencil.getHorizontalAngle() + irr::core::vector3df(0,((rand() % 4) * 90) + ((rand() % 10) - 5),0));
			NewHouse->addShadowVolumeSceneNode(0,0);
			Hice.push_back(NewHouse);
		}
	}
}

void Game::ApplyCollisions(){
	if(NotBadHorse){
		NotBadHorse->Camera->removeAnimators();
		
		NotBadHorse->ApplyCollsWith(Ground);
		
		for(std::vector<irr::scene::IMeshSceneNode*>::iterator IT = Hice.begin();IT < Hice.end();++IT){
			NotBadHorse->ApplyCollsWith(*IT);
		}
	}
}

int main(){
	Game::SDAS.Run();
	
	return EXIT_SUCCESS;
}