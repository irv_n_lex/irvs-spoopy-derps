#include <string>

#include "Game.hpp"
#include "Objects.hpp"

irr::u32 OBJECT_ID = 2;

Object::Object(ObjectType NewType,int NewVar,irr::core::vector3df NewPosition,irr::core::vector3df NewRotation)
:Type(NewType),Var(NewVar){
	switch(Type){
		case CANDY_OBJECT:
			switch(Var){
				case CANDY_CORNZ_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(CANDY_CORNZ_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					break;
				case LOLLIPOP_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(LOLLIPOP_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					
					switch(rand() % 2){
						case 0:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(LOLLI_RED_TEXTURE));
							break;
						case 1:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(LOLLI_GREEN_TEXTURE));
							break;
					}
					
					break;
				case CANDEH_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(CANDEH_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					
					switch(rand() % 4){
						case 0:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(CANDEH_RED_TEXTURE));
							break;
						case 1:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(CANDEH_PURPLE_TEXTURE));
							break;
						case 2:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(CANDEH_YELLOW_TEXTURE));
							break;
						case 3:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(CANDEH_AQUA_TEXTURE));
							break;
					}
					
					break;
				case SUGAR_STICK_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(SUGAH_STICK_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					
					switch(rand() % 3){
						case 0:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(STICK_PINK_TEXTURE));
							break;
						case 1:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(STICK_YELLOW_TEXTURE));
							break;
						case 2:
							Mesh->setMaterialTexture(0,Game::SDAS.Stuffs->GetTexture(STICK_GREEN_TEXTURE));
							break;
					}
					
					break;
			}
			
			Mesh->setScale(irr::core::vector3df(0.2,0.2,0.2));
			Collider = Mesh;
			
			SetCollSize(irr::core::vector3df(0.1,0.1,0.1));
			
			break;
		case PUMPKIN_OBJECT:
			switch(Var){
				case PUMPKIN_YAY_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(PUMPKIN_YAY_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					break;
				case PUMPKIN_OH_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(PUMPKIN_OH_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					break;
				case PUMPKIN_EVIL_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(PUMPKIN_EVIL_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					break;
				case PUMPKIN_MOON_TYPE:
					Mesh = Game::SDAS.Scene->addMeshSceneNode(Game::SDAS.Stuffs->GetMesh(PUMPKIN_MOON_MESH),0,OBJECT_ID,NewPosition,NewRotation);
					break;
			}
			
			Mesh->addShadowVolumeSceneNode(0,0);
			Collider = Mesh;
			
			Mesh->setScale(irr::core::vector3df(0.8,0.8,0.8));
			SetCollSize(irr::core::vector3df(0.35,0.35,0.35));
			break;
	}
	
	SetSlidiness(0.1);
	ApplyCollsWith(Game::SDAS.Ground);
}

Object::~Object(){
	Mesh->remove();
}