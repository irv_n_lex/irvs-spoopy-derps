#include <cstdlib>
#include <iostream>

#include "Game.hpp"
#include "Objects.hpp"
#include "Player.hpp"

const irr::f32 PLAYER_REACH = 1.0,PLAYER_SPEED = 10.0f;//1.5;

Player::Player(irr::core::vector3df NewPosition)
:CandyCount(0),Focused(0),OldFocus(0),Pumpkin(0){
	Camera = Game::SDAS.Scene->addCameraSceneNode(0,NewPosition,irr::core::vector3df(0,0,1),4);
	Camera->setNearValue(0.3);
	Collider = Camera;
	
	CameraMovementTarget = new PureNode(Camera,Game::SDAS.Scene,0);
	
	CameraTarget = new PureNode(CameraMovementTarget,Game::SDAS.Scene,0);
	CameraTarget->setPosition(irr::core::vector3df(0,0,1));
	
	SetCollSize(irr::core::vector3df(0.2,0.4,0.2));
	SetCollTranslation(irr::core::vector3df(0,0.2,0));
	SetSlidiness(0.003);
	
	PumpkinPivot = new PureNode(Game::SDAS.Scene->getRootSceneNode(),Game::SDAS.Scene,4);
}

void Player::Update(){
	TargetRotation.X += 2400 * (Game::SDAS.DeltaCursorPosition.Y / 0.25) * (Game::SDAS.DeltaTime / 1000);
	TargetRotation.Y += 2400 * (Game::SDAS.DeltaCursorPosition.X / 0.25) * (Game::SDAS.DeltaTime / 1000);
	
	TargetRotation.X -= ((TargetRotation.X > 89) * (TargetRotation.X - 89)) + ((TargetRotation.X < -89) * (TargetRotation.X + 89));
	
	Rotation += ((TargetRotation - Rotation) * 0.95 * (Game::SDAS.DeltaTime / 100));
	
	Camera->setRotation(irr::core::vector3df(0,Rotation.Y,0));
	CameraMovementTarget->setPosition(irr::core::vector3df(
		(Game::SDAS.Input->KeyDown(irr::KEY_KEY_D) - Game::SDAS.Input->KeyDown(irr::KEY_KEY_A)) * (PLAYER_SPEED / 2) * (1 - ((Pumpkin != 0) * 0.75)) * (Game::SDAS.DeltaTime / 1000),
		0,
		((Game::SDAS.Input->KeyDown(irr::KEY_KEY_W) - Game::SDAS.Input->KeyDown(irr::KEY_KEY_S)) != 0) * ((Game::SDAS.Input->KeyDown(irr::KEY_KEY_W) * PLAYER_SPEED) - (Game::SDAS.Input->KeyDown(irr::KEY_KEY_S) * (PLAYER_SPEED / 2))) * (1 - ((Pumpkin != 0) * 0.75)) * (Game::SDAS.DeltaTime / 1000)
	));
	
	CameraMovementTarget->updateAbsolutePosition();
	Camera->setPosition(CameraMovementTarget->getAbsolutePosition());
	
	CameraMovementTarget->setPosition(irr::core::vector3df(0,0,0));
	
	Camera->setRotation(Rotation);
	CameraTarget->updateAbsolutePosition();
	Camera->setTarget(CameraTarget->getAbsolutePosition());
	
	PumpkinPivot->setPosition(Camera->getPosition());
	PumpkinPivot->setRotation(PumpkinPivot->getRotation() + ((Camera->getRotation() - PumpkinPivot->getRotation()) * 0.95 * (Game::SDAS.DeltaTime / 150)));
	
	if(Pumpkin){
		if(Game::SDAS.Input->KeyHit(irr::KEY_SPACE)){
			Pumpkin->Mesh->setParent(Game::SDAS.Scene->getRootSceneNode());
			Pumpkin->Mesh->setPosition(Camera->getPosition());
			Pumpkin->Mesh->setRotation(irr::core::vector3df(0,Camera->getRotation().Y,0));
			Pumpkin->ApplyCollsWith(Game::SDAS.Ground);
			
			Pumpkin = 0;
		}
	}else{
		Focused = Game::SDAS.Scene->getSceneCollisionManager()->getSceneNodeFromCameraBB(Camera,0b00000010);
		
		if(Focused){
			Focused->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			
			if(Game::SDAS.Input->MouseHit(MOUSE_LEFT)){
				for(std::vector<Object*>::iterator IT = Game::SDAS.Objects.begin();IT < Game::SDAS.Objects.end();++IT){
					if((*IT)->Mesh == Focused){
						Focused->setMaterialFlag(irr::video::EMF_LIGHTING,true);
						
						switch((*IT)->Type){
							case CANDY_OBJECT:
								++CandyCount;
								
								if(OldFocus == Focused){
									OldFocus = 0;
								}
								
								Focused = 0;
								
								delete *IT;
								Game::SDAS.Objects.erase(IT);
								
								break;
							case PUMPKIN_OBJECT:
								Pumpkin = *IT;
								Pumpkin->Mesh->removeAnimators();
								Pumpkin->Mesh->setParent(PumpkinPivot);
								Pumpkin->Mesh->setPosition(irr::core::vector3df(0,0,0));
								Pumpkin->Mesh->setRotation(irr::core::vector3df(0,0,180));
								
								break;
						}
					}
				}
			}
		}
		
		if(OldFocus && OldFocus != Focused){
			OldFocus->setMaterialFlag(irr::video::EMF_LIGHTING,true);
			OldFocus = 0;
		}
		
		OldFocus = Focused;
	}
	
	if(Game::SDAS.Input->KeyHit(irr::KEY_KEY_E)){
		Game::SDAS.SpawnObject(PUMPKIN_OBJECT,rand() % PUMPKIN_TYPE_COUNT,Camera->getPosition() + irr::core::vector3df(0,3,0));
	}else if(Game::SDAS.Input->KeyHit(irr::KEY_KEY_Q)){
		Game::SDAS.SpawnObject(CANDY_OBJECT,rand() % CANDY_TYPE_COUNT,Camera->getPosition() + irr::core::vector3df(0,3,0));
	}
}

irr::f32 Player::GetSecondDeltaPosition(){
	
}